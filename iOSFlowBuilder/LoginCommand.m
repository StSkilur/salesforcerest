//
//  LoginCommand.m
//  iOSFlowBuilder
//
//  Created by Sergey on 03/06/16.
//  Copyright © 2016 Sergey Emelyanov. All rights reserved.
//

#import "AppData.h"
#import "AppModel.h"
#import "LoginCommand.h"

@implementation LoginCommand

#define ERRORMESSAGE @"Invalid username, password, security token; or user locked out."

@synthesize organizationId;
@synthesize portalId;
@synthesize username;
@synthesize password;

- (void) execute
{
   NSString *loginXmlFilename = [[NSBundle mainBundle] pathForResource:@"LoginRequest2" ofType:@"xml"];
   NSString *post = [NSString stringWithContentsOfFile:loginXmlFilename encoding:NSUTF8StringEncoding error:nil];
   post = [post stringByReplacingOccurrencesOfString:@"{organizationId}" withString:organizationId];
   post = [post stringByReplacingOccurrencesOfString:@"{portalId}" withString:portalId];
   post = [post stringByReplacingOccurrencesOfString:@"{username}" withString:username];
   post = [post stringByReplacingOccurrencesOfString:@"{password}" withString:password];   
   if (DEBUG)
   {
      NSLog(@"Login Request:\n%@", post);  
   }
   
   NSURL *loginUrl = [NSURL URLWithString:LoginURL];
   NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:loginUrl];
   [request setHTTPMethod:@"post"];
   [request addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
   [request addValue:@"\"\"" forHTTPHeaderField:@"SOAPAction"];
   [request setHTTPBody:[post dataUsingEncoding:NSUTF8StringEncoding]];
   
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      // ...
                                      [self prosessData:data];
                                  }];
    
    [task resume];
   //NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
   //[connection start];
}

- (void)prosessData:(NSData*)theReceivedData {
    NSString *xml = [[NSString alloc] initWithData:theReceivedData encoding:NSUTF8StringEncoding];
    if (DEBUG)
    {
        NSLog(@"Login Response:\n%@", xml);
    }
    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:theReceivedData];
    [parser setDelegate:self];
    BOOL success = [parser parse];
    if (success)
    {
        if ([[AppModel instance] serverUrl] != nil && [[AppModel instance] userId] != nil && [[AppModel instance] sessionId] != nil) {
            if ([[AppModel instance] serverUrl].length > 0 && [[AppModel instance] userId].length > 0 && [[AppModel instance] sessionId].length > 0) {
                NSLog(@"[[AppModel instance] sessionId]]:\n%@", [[AppModel instance] sessionId]);
                [self postSucceedNotification:NLoginCommandCompleted withDictionary:nil];
            } else {
                [self postFailNotification:NLoginCommandCompleted withMessage:ERRORMESSAGE];
            }
        } else {
            [self postFailNotification:NLoginCommandCompleted withMessage:ERRORMESSAGE];
        }
    }
}

- (void) postCompletedNotification:(BOOL)success withError:(NSError*)error
{
   NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
   [userInfo setObject:[NSNumber numberWithBool:success] forKey:@"success"];
   [userInfo setObject:[error localizedDescription] forKey:@"message"];
   
   [[NSNotificationCenter defaultCenter] postNotificationName:@"NLoginCommandCompleted"
                                                       object:nil 
                                                     userInfo:userInfo];
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
   receivedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{
   [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
   [self postFailNotification:@"NLoginCommandCompleted" withMessage:[error localizedDescription]];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
   NSString *xml = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
   //xml = @"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sf=\"urn:fault.partner.soap.sforce.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soapenv:Body><serverUrl></serverUrl><userId></userId><sessionId></sessionId></soapenv:Body></soapenv:Envelope>";
   //receivedData = [xml dataUsingEncoding:NSUTF8StringEncoding];
   if (DEBUG)
   {
      NSLog(@"Login Response:\n%@", xml);
   }
   
   NSXMLParser *parser = [[NSXMLParser alloc] initWithData:receivedData];
   [parser setDelegate:self];
   BOOL success = [parser parse];
   if (success)
   {
      if ([[AppModel instance] serverUrl] != nil && [[AppModel instance] userId] != nil && [[AppModel instance] sessionId] != nil) {
         if ([[AppModel instance] serverUrl].length > 0 && [[AppModel instance] userId].length > 0 && [[AppModel instance] sessionId].length > 0) {
            NSLog(@"[[AppModel instance] sessionId]]:\n%@", [[AppModel instance] sessionId]);
            [self postSucceedNotification:NLoginCommandCompleted withDictionary:nil];
         } else {
            [self postFailNotification:NLoginCommandCompleted withMessage:@"Invalid username, password, security token; or user locked out."];
         }
      } else {
         [self postFailNotification:NLoginCommandCompleted withMessage:@"Invalid username, password, security token; or user locked out."];
      }
   }
}

#pragma mark - NSXMLParserDelegate

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName 
   namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName 
     attributes:(NSDictionary *)attributeDict
{
   nodeText = [[NSMutableString alloc] initWithString:@""];
}

- (void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
   [nodeText appendString:string];
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
   if ([elementName isEqualToString:@"sf:exceptionMessage"])
   {
      [parser abortParsing];
      [self postFailNotification:NLoginCommandCompleted withMessage:nodeText];
   }
   else if ([elementName isEqualToString:@"serverUrl"])
   {
      NSURL *url = [NSURL URLWithString:nodeText];
      NSString *serverUrl = [NSString stringWithFormat:@"%@://%@", url.scheme, url.host];      
      [[AppModel instance] setServerUrl:serverUrl];
   }
   else if ([elementName isEqualToString:@"userId"])
   {
      [[AppModel instance] setUserId:nodeText];
   }
   else if ([elementName isEqualToString:@"sessionId"])
   {
      [[AppModel instance] setSessionId:nodeText];
   }
   nodeText = nil;
}

@end