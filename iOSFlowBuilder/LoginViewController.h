//
//  LoginViewController.h
//  iOSFlowBuilder
//
//  Created by Sergey on 03/06/16.
//  Copyright © 2016 Sergey Emelyanov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BackgroundHelper.h"
#import "LoginCommand.h"
#import "AppData.h"
#import "GetItems.h"
#import "WebViewInterface.h"

@interface LoginViewController : UIViewController<WebViewInterface>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;

- (IBAction)loginAction:(id)sender;

@end
