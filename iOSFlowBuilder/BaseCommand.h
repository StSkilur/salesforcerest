//
//  BaseCommand.h
//  iOSFlowBuilder
//
//  Created by Sergey on 03/06/16.
//  Copyright © 2016 Sergey Emelyanov. All rights reserved.
//
#import <Foundation/Foundation.h>

@protocol PCommand

- (void) execute;
- (void) requestSucceeded;
- (void) requestFailed:(NSString *)error;

@end

@interface BaseCommand : NSObject<NSURLConnectionDelegate, PCommand> 
{
   NSMutableData *receivedData;
}

- (void) performWebRequest:(NSString *)path withMethod:(NSString *)method andData:(NSData *)data;
- (void) postSucceedNotification:(NSString *)name withDictionary:(NSDictionary *)dictionary;
- (void) postFailNotification:(NSString *)name withMessage:(NSString *)message;

@end
