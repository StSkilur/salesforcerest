//
//  AppDelegate.h
//  iOSFlowBuilder
//
//  Created by Sergey on 03/06/16.
//  Copyright © 2016 Sergey Emelyanov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "BackgroundHelper.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, NSURLSessionDelegate> {
    UIApplication *app;
    NSTimeInterval timeInSeconds;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSURLSession *session;


@end

