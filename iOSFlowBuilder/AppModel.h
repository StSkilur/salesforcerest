//
//  AppModel.h
//  StudioIT
//
//  Created by Dmitry Danilchuk on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface AppModel : NSObject
{   
   NSString *serverUrl;
   NSString *userId;
   NSString *sessionId;
   
   NSString *addressFilterAddressId;
   NSString *addressFilterType;
   NSString *addressFilterName;
   NSString *addressFilterActivity;
   BOOL addressSortAddressId;
   
   NSString *activityFilterDateFrom;
   NSString *activityFilterDateTo;
   NSString *activityFilterEmployee;
   NSString *activityFilterClient;
   NSString *activityFilter;
}

+ (AppModel *) instance;

@property (nonatomic, retain) NSString *serverUrl;
@property (nonatomic, retain) NSString *userId;
@property (nonatomic, retain) NSString *sessionId;
@property (nonatomic, retain) NSString *addressFilterAddressId;
@property (nonatomic, retain) NSString *addressFilterType;
@property (nonatomic, retain) NSString *addressFilterName;
@property (nonatomic, retain) NSString *addressFilterActivity;
@property (nonatomic) BOOL addressSortAddressId;
@property (nonatomic, retain) NSString *activityFilterDateFrom;
@property (nonatomic, retain) NSString *activityFilterDateTo;
@property (nonatomic, retain) NSString *activityFilterEmployee;
@property (nonatomic, retain) NSString *activityFilterClient;
@property (nonatomic, retain) NSString *activityFilter;

@end
