//1  var INSTANCE = jaLib();
var jaLib = function(){
    var __context = this,
        __functionIndexMap = {};
    
    return {
        calliOSFunction : function(functionName, args, successCallback, errorCallback){
        
        },
        receiveMessage : function(event){
        
        }
    };
};

//2 var INSTANCE == jaLib2;
var jaLib2 = {
    __functionIndexMap: {},
    calliOSFunction : function(functionName, args, successCallback, errorCallback){
        var __context = this;
//        __context.__functionIndexMap
        
    },
    receiveMessage : function(event){
        
    }
}

//3.1  var INSTANCE = new jaLib3_1(params);
var jaLib3_1 = function(){
    var __context = this;
    
    __context.__functionIndexMap = {};
    this.__functionIndexMap = {};
    
    __context.calliOSFunction = function(functionName, args, successCallback, errorCallback){
        var __context = this;
        //        __context.__functionIndexMap
        
    },
    
}

//3.2
var jaLib3_2 = function(){
    var __context = this;
    
    __context.__functionIndexMap = {};
    this.__functionIndexMap = {};
    
}

jaLib3_2.prototype.calliOSFunction = function(functionName, args, successCallback, errorCallback){
    
    var __context = this;
    //        __context.__functionIndexMap
    
},



__functionIndexMap = {};
var __context;

function receiveMessage(event)
{
    addToList(event.data);
    return;
}

function calliOSFunction(functionName, args, successCallback, errorCallback)
{
    window.addEventListener("message", receiveMessage, false);
    var url = "js2ios://";
    
    var callInfo = {};
    callInfo.functionname = functionName;
    
    if (successCallback)
    {
        if (typeof successCallback == 'function')
        {
            var callbackFuncName = createCallbackFunction(functionName + "_" + "successCallback", successCallback);
            callInfo.success = callbackFuncName;
        }
        else
            callInfo.success = successCallback;
    }
    
    if (errorCallback)
    {
        if (typeof errorCallback == 'function')
        {
            var callbackFuncName = createCallbackFunction(functionName + "_" + "errorCallback", errorCallback);
            callInfo.error = callbackFuncName;
        }
        else
            callInfo.error = errorCallback;
    }
    
    if (args)
    {
        callInfo.args = args;
    }
    
    url += JSON.stringify(callInfo)
    
    //eval(callbackFuncName + "({message:'This is a test<br>'})");
    
    var iFrame = createIFrame(url);
    //remove the frame now
    iFrame.parentNode.removeChild(iFrame);
}

function createCallbackFunction (funcName, callbackFunc)
{
    if (callbackFunc && callbackFunc.name != null && callbackFunc.name.length > 0)
    {
        return callbackFunc.name;
    }
    
    if (typeof __context[funcName+0] != 'function')
    {
        __context[funcName+0] = callbackFunc;
        __functionIndexMap[funcName] = 0;
        return funcName+0
        
    } else
    {
        var maxIndex = __functionIndexMap[funcName];
        var callbackFuncStr = callbackFunc.toString();
        for (var i = 0; i <= maxIndex; i++)
        {
            var tmpName = funcName + i;
            if (__context[tmpName].toString() == callbackFuncStr)
                return tmpName;
        }
        
        var newIndex = ++__functionIndexMap[funcName];
        __context[funcName+newIndex] = callbackFunc;
        return funcName+newIndex;
    }
}

function createIFrame(src)
{
    var rootElm = document.documentElement;
    var newFrameElm = document.createElement("IFRAME");
    newFrameElm.setAttribute("src",src);
    rootElm.appendChild(newFrameElm);
    return newFrameElm;
}
