//
//  BackgroundHelper.m
//  iOSFlowBuilder
//
//  Created by Sergey on 03/06/16.
//  Copyright © 2016 Sergey Emelyanov. All rights reserved.
//

#import "BackgroundHelper.h"

@implementation BackgroundHelper

+(BOOL)isConnectionAvailable
{
    CFNetDiagnosticRef dReference;
    dReference = CFNetDiagnosticCreateWithURL (NULL, (__bridge CFURLRef)[NSURL URLWithString:@"www.apple.com"]);
    
    CFNetDiagnosticStatus status;
    status = CFNetDiagnosticCopyNetworkStatusPassively (dReference, NULL);
    
    CFRelease (dReference);
    
    if ( status == kCFNetDiagnosticConnectionUp )
    {
        NSLog (@"Connection is Available");
        return YES;
    }
    else
    {
        NSLog (@"Connection is down");
        return NO;
    }
}

+ (void) createUpload {
    NSMutableURLRequest *mutRequest = [self configureRequest:@"test"];
    NSURLResponse *response;
    NSError *error;
    
    NSData *aData = [NSURLConnection  sendSynchronousRequest:mutRequest
                                          returningResponse:&response
                                                      error:&error];
    
    if (aData) {
        //NSString *jsonReturn = [[NSString alloc] initWithData:aData encoding: NSASCIIStringEncoding];
        NSLog(@"jsonReturn");
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger statusCode = [httpResponse statusCode];
        if (statusCode == 200) {
            NSLog(@"jsonReturn !!!");
        }
    }
}

+ (NSMutableURLRequest*)configureRequest:(NSString*)theString {
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc] init];
    NSString *urlString = @"https://google.com";
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"*/*" forHTTPHeaderField:@"Accept"];
    [request setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:@"ru,en-US;q=0.8,en;q=0.6" forHTTPHeaderField:@"Accept-Language"];
    [request addValue:[NSString stringWithFormat:@"%li", [theString length]] forHTTPHeaderField:@"Content-Length"];
    NSData *sendingData = [theString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:sendingData];
    return request;
}

@end
