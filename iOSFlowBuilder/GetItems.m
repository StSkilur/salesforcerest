//
//  GetItems.m
//  iOSFlowBuilder
//
//  Created by Sergey on 17/06/16.
//  Copyright © 2016 Sergey Emelyanov. All rights reserved.
//

#import "GetItems.h"

@implementation GetItems

+ (GetItems *) create
{
    return [[GetItems alloc] init];
}

+ (GetItems *) createWithDateFrom:(NSString *)dateFrom
                                    andDateTo:(NSString *)dateTo
                                  andEmployee:(NSString *)employeeId
                                    andClient:(NSString *)clientId
                                    andFilter:(NSString *)filterId
{
    GetItems *cmd = [self create];
    return cmd;
}

#pragma mark - PCommand

- (void) execute
{
    NSString *itemId = @"";
    NSString *url = [NSString stringWithFormat:@"/services/apexrest/WR_BPM__Item__c?itemId=%@",
                     itemId ? itemId : @""];
    url = [NSString stringWithFormat:@"/services/apexrest/WR_BPM__Item__c/"];
    [self performWebRequest:url withMethod:@"get" andData:nil];
}

- (void) requestSucceeded
{
    //reseivedData
    
    NSError *error;
    NSArray *json = [NSJSONSerialization JSONObjectWithData:receivedData
                                                         options:0
                                                           error:&error];
    
    NSMutableArray *WBItemList = [[NSMutableArray alloc] init];
    
    if ([json isKindOfClass:[NSArray class]]) {
        //NSArray *yourStaffDictionaryArray = json[@"items"];
        for (NSDictionary *dictionary in json) {
            WBItem *item = [[WBItem alloc] init];
            item.Id = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"Id"]];
            item.Name = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"Name"]];
            item.WR_BPM__Is_System__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Is_System__c"]];
            item.WR_BPM__Name_Value__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Name_Value__c"]];
            item.WR_BPM__Parent__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Parent__c"]];
            item.WR_BPM__Parent_Path__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Parent_Path__c"]];
            item.WR_BPM__Parent_Sub_Path__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Parent_Sub_Path__c"]];
            item.WR_BPM__Path__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Path__c"]];
            item.WR_BPM__Source__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Source__c"]];
            item.WR_BPM__Source_Flow__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Source_Flow__c"]];
            item.WR_BPM__Source_Flow_Name__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Source_Flow_Name__c"]];
            item.WR_BPM__Source_Form__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Source_Form__c"]];
            item.WR_BPM__Source_Form_Name__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Source_Form_Name__c"]];
            item.WR_BPM__Source_Id__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Source_Id__c"]];
            item.WR_BPM__Source_Object_Id__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Source_Object_Id__c"]];
            item.WR_BPM__Source_Process__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Source_Process__c"]];
            item.WR_BPM__Source_Process_Name__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Source_Process_Name__c"]];
            item.WR_BPM__Source_Type__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Source_Type__c"]];
            item.WR_BPM__Sub_Path__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Sub_Path__c"]];
            item.WR_BPM__Type__c = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"WR_BPM__Type__c"]];
            [WBItemList addObject:item];
        }
    }
    
    [self postSucceedNotification:@"NGetItemsCommandCompleted" withDictionary:[NSDictionary dictionaryWithObjectsAndKeys:WBItemList, @"items", nil]];
}

- (void) requestFailed:(NSString *)error
{
    [self postFailNotification:@"NGetItemsCommandCompleted" withMessage:error];
}

@end
