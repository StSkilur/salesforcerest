//
//  BaseCommand.m
//  iOSFlowBuilder
//
//  Created by Sergey on 03/06/16.
//  Copyright © 2016 Sergey Emelyanov. All rights reserved.
//

#import "AppData.h"
#import "AppModel.h"
#import "BaseCommand.h"

@implementation BaseCommand

- (void) performWebRequest:(NSString *)path withMethod:(NSString *)method andData:(NSData *)data
{
   NSString *url = [NSString stringWithFormat:@"%@%@", [[AppModel instance] serverUrl], path];
   if (DEBUG)
   {
      NSLog(@"%@ Url:\n%@", [self class], url);
      if (data) 
      {
         //NSString *text = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
         //NSLog(@"%@ Post:\n%@", [self class], text);  
      }
   }
   
   NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:method];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
   NSLog(@"\n");
   [request setValue:[@"OAuth " stringByAppendingString:[[AppModel instance] sessionId]] forHTTPHeaderField:@"Authorization"];
   if (data) [request setHTTPBody:data];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      [self prosessData:data];
                                  }];
    
    [task resume];
   /*
   NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
   [connection start];   
    */
}

- (void)prosessData:(NSData*)theReceivedData {
    receivedData = [theReceivedData mutableCopy];
    NSString *text = [[NSString alloc] initWithData:theReceivedData encoding:NSUTF8StringEncoding];
    if (DEBUG) NSLog(@"%@ Response:\n%@", [self class], text);
    if ([text length] > 2) {
        NSError *error;
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:theReceivedData
                                                             options:0
                                                               error:&error];
        if ([json isKindOfClass:[NSArray class]])
        {
            NSObject *arrayElement = [(NSArray *)json objectAtIndex:0];
            if ([arrayElement isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *dictionary = (NSDictionary *) arrayElement;
                if ([dictionary objectForKey:@"message"] && [dictionary objectForKey:@"errorCode"])
                {
                    [self requestFailed:[NSString stringWithFormat:@"%@: %@",
                                         [dictionary objectForKey:@"errorCode"],
                                         [dictionary objectForKey:@"message"]]];
                    return;
                }
            }
        }
    }
    
    [self requestSucceeded];
}

- (void) postSucceedNotification:(NSString *)name withDictionary:(NSDictionary *)dictionary
{
   NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:dictionary];
   [userInfo setObject:[NSNumber numberWithBool:YES] forKey:@"success"];
   
   [[NSNotificationCenter defaultCenter] postNotificationName:name
                                                       object:nil 
                                                     userInfo:userInfo];
}

- (void) postFailNotification:(NSString *)name withMessage:(NSString *)message
{
   NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
   [userInfo setObject:[NSNumber numberWithBool:NO] forKey:@"success"];
   [userInfo setObject:message forKey:@"message"];
   
   [[NSNotificationCenter defaultCenter] postNotificationName:name
                                                       object:nil 
                                                     userInfo:userInfo];
}

#pragma mark - Command protocol

- (void)execute 
{
}

- (void) requestSucceeded
{
}

- (void)requestFailed:(NSString *)error
{   
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
   receivedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{
   [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
   [self requestFailed:error.description];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
   NSString *text = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];   
   if (DEBUG) NSLog(@"%@ Response:\n%@", [self class], text);
    if ([text length] > 2) {
        NSError *error;
       
       NSDictionary *json = [NSJSONSerialization JSONObjectWithData:receivedData
                                                            options:0
                                                              error:&error];
      if ([json isKindOfClass:[NSArray class]])
      {
         NSObject *arrayElement = [(NSArray *)json objectAtIndex:0];
         if ([arrayElement isKindOfClass:[NSDictionary class]])
         {
            NSDictionary *dictionary = (NSDictionary *) arrayElement;
            if ([dictionary objectForKey:@"message"] && [dictionary objectForKey:@"errorCode"])
            {
               [self requestFailed:[NSString stringWithFormat:@"%@: %@", 
                                    [dictionary objectForKey:@"errorCode"], 
                                    [dictionary objectForKey:@"message"]]];
               return;
            }
         }
      }
   }
   
   [self requestSucceeded];
}

@end
