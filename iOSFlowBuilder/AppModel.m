//
//  AppModel.m
//  StudioIT
//
//  Created by Dmitry Danilchuk on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppModel.h"

@implementation AppModel

@synthesize serverUrl;
@synthesize userId;
@synthesize sessionId;
@synthesize addressFilterAddressId;
@synthesize addressFilterType;
@synthesize addressFilterName;
@synthesize addressFilterActivity;
@synthesize addressSortAddressId;
@synthesize activityFilterDateFrom;
@synthesize activityFilterDateTo;
@synthesize activityFilterEmployee;
@synthesize activityFilterClient;
@synthesize activityFilter;

+ (AppModel *) instance 
{
	static AppModel *instance;
	
	@synchronized(self) 
   {
		if (!instance)  
      {
          instance = [[AppModel alloc] init];
          [instance setAddressFilterAddressId:@""];
          [instance setAddressFilterType:@""];
          [instance setAddressFilterName:@""];
          [instance setAddressFilterActivity:@"true"];
          [instance setAddressSortAddressId:YES];
          [instance setActivityFilterDateFrom:@""];
          [instance setActivityFilterDateTo:@""];
          [instance setActivityFilterEmployee:@""];
          [instance setActivityFilterClient:@""];
          [instance setActivityFilter:@""];
		}
	}
	
	return instance;
}

@end
