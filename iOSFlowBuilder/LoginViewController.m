//
//  LoginViewController.m
//  iOSFlowBuilder
//
//  Created by Sergey on 03/06/16.
//  Copyright © 2016 Sergey Emelyanov. All rights reserved.
//

#import "LoginViewController.h"
#import "WebViewDelegate.h"

#define OrganizationId @""
#define PortalId @""
#define Username @""
#define Password @""

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txt1;
- (IBAction)onAdd:(id)sender;
@property WebViewDelegate *webViewDelegate;
@end

@implementation LoginViewController

- (id) processFunctionFromJS:(NSString *) name withArgs:(NSArray*) args error:(NSError **) error
{
    if ([name compare:@"loadList" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        NSArray *listElements = @[];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:listElements options:0 error:nil];
        
        NSString *result = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        return result;
    }
    
    return nil;
}

- (IBAction)onAdd:(id)sender {
    NSDictionary *dict = @{@"newListItem":self.txt1.text};
    
    [self.webViewDelegate callJSFunction:@"addToList" withArgs:dict];
    
}

 #pragma mark - Controller


- (void)viewDidLoad {
    [super viewDidLoad];
    self.backgroundTask = UIBackgroundTaskInvalid;
    [BackgroundHelper isConnectionAvailable];
    
    self.webViewDelegate = [[WebViewDelegate alloc] initWithWebView:self.webView withWebViewInterface:self];
    
    self.webView.scrollView.scrollEnabled = false;
    [self.webViewDelegate loadPage:@"index.html" fromFolder:@"www"];
}

- (void)viewDidAppear:(BOOL)animated {
    self.backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        
        [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTask];
        self.backgroundTask = UIBackgroundTaskInvalid;
    }];
    
    [self getDataFromServer];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTask];
        self.backgroundTask = UIBackgroundTaskInvalid;
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) getDataFromServer {
    
    [BackgroundHelper isConnectionAvailable];
}

- (void)getItemsDataResult:(NSNotification*)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    for (WBItem *item in [notification.userInfo objectForKey:@"items"]) {
        NSDictionary *dict = @{@"newListItem":[item.Name copy]};
        [self performSelectorOnMainThread:@selector(setNewItem:) withObject:dict waitUntilDone:NO];
    }
}

- (void)setNewItem:(NSDictionary*)dict {
    [self.webViewDelegate callJSFunction:@"postMessage" withArgs:dict];
}

- (void)getItemsData:(NSNotification*)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if ([[[notification userInfo] objectForKey:@"success"] boolValue]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getItemsDataResult:) name:@"NGetItemsCommandCompleted" object:nil];
        GetItems *command = [[GetItems alloc] init];
        [command execute];
    } else {
        //TODO add alert
        NSLog(@"%@",[[notification userInfo] objectForKey:@"message"]);
    }
}

- (IBAction)loginAction:(id)sender {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getItemsData:) name:NLoginCommandCompleted object:nil];
    LoginCommand *command = [[LoginCommand alloc] init];
    [command setOrganizationId:OrganizationId];
    [command setPortalId:PortalId];
    [command setUsername:Username];
    [command setPassword:Password];
    [command execute];
}

@end
