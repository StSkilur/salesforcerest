//
//  LoginCommand.h
//  iOSFlowBuilder
//
//  Created by Sergey on 03/06/16.
//  Copyright © 2016 Sergey Emelyanov. All rights reserved.
//

#import "BaseCommand.h"

@interface LoginCommand : BaseCommand<NSXMLParserDelegate>
{
   NSString *portalId;
   NSString *organizationId;
   NSString *username;
   NSString *password;
   
   NSMutableString *nodeText;
}

@property (nonatomic, retain) NSString *portalId;
@property (nonatomic, retain) NSString *organizationId;
@property (nonatomic, retain) NSString *username;
@property (nonatomic, retain) NSString *password;

- (void) postCompletedNotification:(BOOL)success withError:(NSError*)error;

@end