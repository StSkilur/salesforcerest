//
//  WBItem.m
//  iOSFlowBuilder
//
//  Created by Sergey on 22/06/16.
//  Copyright © 2016 Sergey Emelyanov. All rights reserved.
//

#import "WBItem.h"

@implementation WBItem

@synthesize Id;
@synthesize Name;
@synthesize WR_BPM__Is_System__c;
@synthesize WR_BPM__Name_Value__c;
@synthesize WR_BPM__Parent__c;
@synthesize WR_BPM__Parent_Path__c;
@synthesize WR_BPM__Parent_Sub_Path__c;
@synthesize WR_BPM__Path__c;
@synthesize WR_BPM__Source__c;
@synthesize WR_BPM__Source_Flow__c;
@synthesize WR_BPM__Source_Flow_Name__c;
@synthesize WR_BPM__Source_Form__c;
@synthesize WR_BPM__Source_Form_Name__c;
@synthesize WR_BPM__Source_Id__c;
@synthesize WR_BPM__Source_Object_Id__c;
@synthesize WR_BPM__Source_Process__c;
@synthesize WR_BPM__Source_Process_Name__c;
@synthesize WR_BPM__Source_Type__c;
@synthesize WR_BPM__Sub_Path__c;
@synthesize WR_BPM__Type__c;

@end
