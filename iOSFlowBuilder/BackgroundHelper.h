//
//  BackgroundHelper.h
//  iOSFlowBuilder
//
//  Created by Sergey on 03/06/16.
//  Copyright © 2016 Sergey Emelyanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackgroundHelper : NSObject

+(BOOL)isConnectionAvailable;
+ (NSURLSessionConfiguration*)backgroundSessionConfig;
+ (void) createUpload;

@end
