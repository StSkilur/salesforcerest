//
//  AppData.h
//  StudioIT
//
//  Created by Dmitry Danilchuk on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define DEBUG 1

//#define LoginURL @"https://test.salesforce.com/services/Soap/u/24.0"   //sandbox
//#define OrganizationID @"00DL0000000BoChMAK" //sandbox
//#define PortalID @"0608000000067yi"       //sandbox
//
//https://login.salesforce.com/?un=work-relay-process-ltng-dev@silvertreesystems.com&pw=workrelay4ever
//
#define LoginURL @"https://login.salesforce.com/services/Soap/u/24.0"    //production
#define OrganizationID @"00D36000000uMsx"   //production
#define PortalID @"06036000000LkrE"         //production

#define NDeleteActivityCommandCompleted @"NDeleteActivityCommandCompleted"
#define NDeleteAddressCommandCompleted @"NDeleteAddressCommandCompleted"
#define NDeleteContactCommandCompleted @"NDeleteContactCommandCompleted"
#define NGetAddressesCommandCompleted @"NGetAddressesCommandCompleted"
#define NGetAddressesPickListsCommandCompleted @"NGetAddressesPickListsCommandCompleted"
#define NGetActivitiesCommandCompleted @"NGetActivitiesCommandCompleted"
#define NGetActivitiesPickListsCommandCompleted @"NGetActivitiesPickListsCommandCompleted"
#define NLoginCommandCompleted @"NLoginCommandCompleted"
#define NSaveActivityCommandCompleted @"NSaveActivityCommandCompleted"
#define NSaveAddressCommandCompleted @"NSaveAddressCommandCompleted"
#define NSaveContactCommandCompleted @"NSaveContactCommandCompleted"

#define NCellTextControlEndEditing @"NCellTextControlEndEditing"
#define NCellTextControlValueChanged @"NCellTextControlValueChanged"
#define NCellTextControlValueEndEdit @"NCellTextControlValueEndEdit"

#define NAddressesShouldBeFiltered @"NAddressesShouldBeFiltered"
#define NAddressesShouldBeSorted @"NAddressesShouldBeSorted"

#define NActivitiesShouldBeFiltered @"NActivitiesShouldBeFiltered"
#define NisActiveUpdated @"NisActiveUpdated"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)