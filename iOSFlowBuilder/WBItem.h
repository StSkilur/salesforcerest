//
//  WBItem.h
//  iOSFlowBuilder
//
//  Created by Sergey on 22/06/16.
//  Copyright © 2016 Sergey Emelyanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WBItem : NSObject

@property (nonatomic, retain) NSString *Id;
@property (nonatomic, retain) NSString *Name;
@property (nonatomic, retain) NSString *WR_BPM__Is_System__c;
@property (nonatomic, retain) NSString *WR_BPM__Name_Value__c;
@property (nonatomic, retain) NSString *WR_BPM__Parent__c;
@property (nonatomic, retain) NSString *WR_BPM__Parent_Path__c;
@property (nonatomic, retain) NSString *WR_BPM__Parent_Sub_Path__c;
@property (nonatomic, retain) NSString *WR_BPM__Path__c;
@property (nonatomic, retain) NSString *WR_BPM__Source__c;
@property (nonatomic, retain) NSString *WR_BPM__Source_Flow__c;
@property (nonatomic, retain) NSString *WR_BPM__Source_Flow_Name__c;
@property (nonatomic, retain) NSString *WR_BPM__Source_Form__c;
@property (nonatomic, retain) NSString *WR_BPM__Source_Form_Name__c;
@property (nonatomic, retain) NSString *WR_BPM__Source_Id__c;
@property (nonatomic, retain) NSString *WR_BPM__Source_Object_Id__c;
@property (nonatomic, retain) NSString *WR_BPM__Source_Process__c;
@property (nonatomic, retain) NSString *WR_BPM__Source_Process_Name__c;
@property (nonatomic, retain) NSString *WR_BPM__Source_Type__c;
@property (nonatomic, retain) NSString *WR_BPM__Sub_Path__c;
@property (nonatomic, retain) NSString *WR_BPM__Type__c;

@end
